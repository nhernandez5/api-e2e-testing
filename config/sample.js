module.exports = {
  db: {
    host: process.env.APP_DB_HOST || 'localhost',
    user: process.env.APP_DB_USER || 'nestor',
    password: process.env.APP_DB_PASSWORD || 'mysecretpassword',
    name: process.env.APP_DB_NAME || 'nestor',
  },
  app: {
    externalUrl: process.env.APP_EXTERNAL_URL || 'https://api.sendgrid.com/v3/' ,
  },
}
