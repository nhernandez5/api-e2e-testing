const logger = require('./logger')(),
  path       = require("path");

module.exports = {
  init: function() {
    const config = require('../config/sample');
    this.knex = require('knex')({
      client: 'pg',
      connection: {
        host: config.db.host,
        user: config.db.user,
        password: config.db.password,
        database: config.db.name,
      },
    });
    this.postgres = require('bookshelf')(this.knex);
  },
  loadModels: function() {
    // load all models
    try {
      const models = require('require-all')({
        dirname: path.join(__dirname, '../models/'),
        filter: /^(.+)\.js$/,
        recursive: true,
      });
      logger.info('Loaded all Postgres models');

    } catch(e) {
      logger.error(`Failed to load all Postgres models!\n${e}`);
      logger.error(e.stack);
    }
  },
};
