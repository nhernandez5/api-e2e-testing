const { postgres } = require('../utils/database');

module.exports = postgres.model('User', {
  tableName: 'users',
  tasks: function() {
    return this.hasMany('Task');
  }
});
