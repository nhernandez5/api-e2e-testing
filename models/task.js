const { postgres } = require('../utils/database');

module.exports = postgres.model('Task', {
  tableName: 'tasks',
  user: function() {
    return this.belongsTo('User');
  }
});
