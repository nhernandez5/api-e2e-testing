const restify = require('restify'),
  plugins     = restify.plugins,
  logger      = require('../utils/logger')();

class MockServer {
  constructor() {
    this.requests = [];
    this.status = 404;
    this.responseBody = {};

    this.app = restify.createServer({ name: `Mock API Server ${process.pid}` });
    this.app.pre(restify.pre.sanitizePath());
    this.app.use(plugins.jsonBodyParser({ mapParams: true }));
    this.app.use(plugins.acceptParser(this.app.acceptable));
    this.app.get("*", (req, res) => {
      this.requests.push(req);
      res.send(this.status, this.responseBody);
    });

    this.app.post("*", (req, res) => {
      this.requests.push(req);
      res.send(this.status, this.responseBody);
    });
  }

  async init() {
    const MOCK_SERVER_PORT = process.env.MOCK_SERVER_PORT || 8002;
    this.server = await this.app.listen(MOCK_SERVER_PORT, () => {
      logger.info(`${this.app.name} listening at port ${MOCK_SERVER_PORT}`);
    });
  }

  setupMock(status, body) {
    this.status = status;
    this.responseBody = body;
  }

  teardownMock() {
    if (this.server) {
      this.server.close();
      delete this.server;
    }
  }

  resetRequests() {
    this.requests = [];
  }

  getRequests() {
    return this.requests;
  }
}

module.exports = MockServer;
