const fetch    = require('node-fetch'),
  config       = require('../../config/sample'),
  logger       = require('../../utils/logger')(),
  { postgres } = require('../../utils/database'),
  User         = postgres.model('User');

async function validateEmail(email) {
  const data = { email, source: 'signup' };
  const body = JSON.stringify(data);
  const res = await fetch(`${config.app.externalUrl}/validations/email`, { method: 'POST', body: body });

  if(res.status !== 200) return false;

  const json = await res.json();
  return json.verdict === 'Valid';
}

module.exports = function (server) {
  server.get('/user/:id', async function (req, res, next) {
    const query = { id: req.params.id };
    User
      .where(query)
      .fetch({
        withRelated: ['tasks'], require: false,
      })
      .then((user) => {
        if (user) {
          user = user.toJSON();
          res.send(200, { ...user });
          next();

        } else {
          res.send(404);
          next();
        }
      })
      .catch((err) => {
        logger.error(`Failed to fetch user!\nError: ${err}`);
        res.send(500, { msg: 'Failed to fetch user!' });
        next();
      });
  });

  server.get('/users', function (req, res, next) {
    User
      .fetchAll()
      .then((users) => res.send(200, { users }))
      .catch((err) => {
        logger.error(`Failed to fetch all users!\nError: ${err}`);
        res.send(500, { msg: 'Failed to fetch users!' });
        next();
      });
  });

  server.post('/user', async function (req, res, next) {
      const { email, name } = req.body;
      const data = { email, name };

      const isValidUser = await validateEmail(email);
      if(!isValidUser) {
        return res.send(403);
      }

      User
        .forge(data)
        .save()
        .then((user) => {
          user = user.toJSON();
          return res.send(201, { ...user });
        })
        .catch((err) => {
          logger.error(`Failed to create user!\nError: ${err}`);
          res.send(500, { msg: 'Failed to create user!' });
          next();
        });
  });
};
