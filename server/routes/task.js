const logger   = require('../../utils/logger')(),
  { postgres } = require('../../utils/database'),
  Task = postgres.model('Task');

module.exports = function (server) {
  server.post('/task', async function(req, res, next) {
    const { name, description, user_id } = req.body;
    const data = { name, description, user_id: parseInt(user_id, 10), };

    Task
      .forge(data)
      .save()
      .then((task) => {
        task = task.toJSON();
        return res.send(201, { ...task });
      })
      .catch((err) => {
        logger.error(`Failed to create task!\nError: ${err}`);
        res.send(500, { msg: 'Failed to create task!' });
        next();
      });
  });
};
