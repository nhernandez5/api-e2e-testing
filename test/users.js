const chai   = require('chai'),
  chaiHttp   = require('chai-http'),
  should     = chai.should(),
  { expect } = chai,
  MockServer = require('../server/mock');

const SERVER_URL = process.env.APP_URL || 'http://localhost:8000';
const TEST_USER = {
  email: 'john@doe.com',
  name: 'John',
};

chai.use(chaiHttp);

// Mock the third-party email validation service
const mock = new MockServer();

let createdUserID;

describe("Users", () => {
  before(async () => await mock.init());

  after(() => {
    mock.teardownMock();
  });

  beforeEach(() => (mock.resetRequests()));

  it("should create a new user", done => {
    mock.setupMock(200, { verdict: 'Valid' });

    chai
      .request(SERVER_URL)
      .post('/user')
      .send(TEST_USER)
      .end((err, res) => {
        if (err) {
          done(err);

        } else {
          res.should.have.status(201);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('email');
          res.body.should.have.property('name');
          res.body.id.should.not.be.null;
          res.body.email.should.equal(TEST_USER.email);
          res.body.name.should.equal(TEST_USER.name);
          createdUserID = res.body.id;

          const mockRequests = mock.getRequests();
          expect(mockRequests).to.have.lengthOf(1);
          expect(mockRequests[0].getPath()).to.equal('/validations/email');
          expect(JSON.parse(mockRequests[0].body)).to.have.property('email');
          expect(JSON.parse(mockRequests[0].body).email).to.equal(TEST_USER.email);

          done();
        }
      });
  });

  it("should get the created user", done => {
    chai
      .request(SERVER_URL)
      .get(`/user/${createdUserID}`)
      .end((err, res) => {
        if (err) {
          done(err);

        } else {
          res.should.have.status(200);
          res.body.should.be.a('object');

          res.body.id.should.equal(createdUserID);
          res.body.email.should.equal(TEST_USER.email);
          res.body.name.should.equal(TEST_USER.name);
          done();
        }
      });
  });

  it("should not create user if mail is spammy", done => {
    mock.setupMock(200, { verdict: 'Invalid' });

    chai
      .request(SERVER_URL)
      .post('/user')
      .send(TEST_USER)
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  });

  it("should not create user if email validation API is down", done => {
    mock.setupMock(404, { verdict: 'Invalid' });

    chai
      .request(SERVER_URL)
      .post('/user')
      .send(TEST_USER)
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  });

  it("should return 404 for missing user", done => {
    chai
      .request(SERVER_URL)
      .get('/user/999')
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
});

const TEST_TASK = {
  name: 'Build an end-to-end test example',
  description: 'Who knows, might motivate you to do more Test Driven Development',
};

describe("Tasks", () => {
  it("should create and assign task to user", done => {
    chai
      .request(SERVER_URL)
      .post('/task')
      .send({ ...TEST_TASK, user_id: createdUserID })
      .end((err, res) => {
        if (err) {
          done(err);

        } else {
          res.should.have.status(201);
          res.should.be.json;
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.should.have.property('name');
          res.body.should.have.property('description');
          res.body.should.have.property('user_id');
          res.body.id.should.not.be.null;
          res.body.name.should.equal(TEST_TASK.name);
          res.body.description.should.equal(TEST_TASK.description);
          res.body.user_id.should.equal(createdUserID);

          done();
        }
      });
  });
});
