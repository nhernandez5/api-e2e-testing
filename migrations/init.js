const database = require('../utils/database'),
  logger       = require('../utils/logger')();
database.init();

const { knex } = database;

logger.info('Creating tables...');

function createPostgresTable(tableName, tableFn) {
  knex.schema
    .hasTable(tableName)
    .then((exists) => {
      if (!exists) {
        logger.info(`Creating '${tableName}' table`);
        return knex.schema.createTable(tableName, tableFn);
      }
    })
    .then(() => {
      logger.info(`'${tableName}' table created succesfully!`);
      process.exit(0);
    })
    .catch(err => {
      logger.error(`Unable to create '${tableName}' table!\nError: ${err}`);
      process.exit(1);
    });
}

createPostgresTable('users', (table) => {
  table.increments();
  table.string("email").notNullable();
  table.string("name").notNullable();
  table.timestamps(true, true);
});
createPostgresTable('tasks', (table) => {
  table.increments();
  table.string("name").notNullable();
  table.string("description").notNullable();
  table.timestamps(true, true);
  table.integer('user_id').unsigned().notNullable();
  table.foreign('user_id').references('id').inTable('users');
});
