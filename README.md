API end-to-end testing with a Restify API Server & Docker Compose

## Running the tests

* Make Sure [Docker](https://docs.docker.com/installation/#installation) is Installed
* From the `docker` directory, run:
```bash
$ docker-compose up --build --abort-on-container-exit
$
```
